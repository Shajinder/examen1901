<?php
/*
    Nombre:Shajinder Singh
    Curso: 2DAW DUAL
    Proyecto:Registro

*/
Class App{

    //constructor
    public function __construct(){
        session_start();
    }

    //fase 1 solicita los datos y si existen los muestra.
    public function fase1(){

        if(isset($_SESSION['nombre'])){
            $nombre = $_SESSION['nombre'];
        }else{
            $nombre = "";
        }

        if(isset($_SESSION['apellidos'])){
            $apellidos = $_SESSION['apellidos'];
        }else{
            $apellidos = "";
        }

        if(isset($_SESSION['email'])){
            $email = $_SESSION['email'];
        }else{
            $email = "";
        }

        require "formulario.html";
    }

    //Fase 2 almacena los datos y los muestra.
    public function fase2(){
        
        $_SESSION['nombre'] = $_POST['nombre'];
        $_SESSION['apellidos'] = $_POST['apellidos'];
        $_SESSION['email'] = $_POST['email'];

        require "datos.html";

    }
    //Fase 3 muestra los modulos y si previamente se han seleccionado.
    public function fase3(){
        require "modulo.html";
    }

    //Fase 4 almacena los modulos y muestra los seleccionados.
    public function fase4(){

        if(isset($_POST['modulos'])){
            $_SESSION['modulos'] = array();
            $_SESSION['modulos'] = $_POST['modulos'];
        }
        require "modulosGuardados.html";
    }
    //Muestra un resumen de datos del usuario, tantos los datos personales como los modulos
    //seleccionados.
    public function fase5(){
        require "ultimafase.html";
    }

    //Elimina la session y muestra la página principal.
    public function flush(){
        session_destroy();
        header("Location:  /registro.php");
    }

}

 //main
 $app = new App();

 if(isset($_GET['method'])){
     $method = $_GET['method'];
 }else{
     //metodo por defecto
     $method = "fase1";
 }

 //si existe el metodo, sino sale de la aplicación.
 if(method_exists($app,$method)){
     $app->$method();
 }else{
     exit('3');
     die('metodo no encontrado.');
 }
?>